#!"C:\Users\Marjune Abellana\AppData\Local\Programs\Python\Python37-32\python.exe"

import inspect
import os
import random

import yaml
from atlassian import Jira
from bs4 import BeautifulSoup
from essential_generators import DocumentGenerator
from selenium.common.exceptions import *
from selenium.webdriver.common.keys import Keys
from cryptography.fernet import *
import traceback
from ditestingsuite import *




class Automation:
    config_path = "./configs/"
    result_path = './results/'
    MAX_ATTEMPTS = 15
    TEST_TICKET_NUMBER = 10
    WAIT_TIME = 10

    def __init__(self):
        url_yaml = "urls.yaml"
        html_yaml = "location.yaml"
        credential_yaml = "login.yaml"
        database_yaml = "database.yaml"
        asset_yaml = "assets.yaml"

        self.urls = self.__get_info_from_file(url_yaml)
        self.elements = self.__get_info_from_file(html_yaml)
        self.logins = self.__get_info_from_file(credential_yaml)
        self.asset = self.__get_info_from_file(asset_yaml)
        # chrome_options = Options()
        # chrome_options.add_argument('--headless')
        # chrome_options.add_argument("--start-maximized")
        # self.browser = webdriver.Chrome(chrome_options=chrome_options)
        # self.browser.maximize_window()
        self.output_database = self.result_path + database_yaml
        # self.actions = ActionChains(self.browser)
        self.project_name = self.asset['test_project_name']
        self.project_key = self.asset['test_project_key']
        self.gen = DocumentGenerator()
        self.admin_username = str(self.asset['admin_username']).strip()
        key = os.environ.get('POSTUPGRADE_MASTER_PASSWORD')
        cipher_suite = Fernet(key)
        self.admin_password  = cipher_suite.decrypt( str.encode( self.asset['admin_password'])).decode('utf-8')
        self.jira = Jira(url=self.asset['base_url'], username=self.admin_username, password=self.admin_password)
        self.main_driver_controller = testingController()
        self.action_enum = actionEnum()
        self.login()

    def __get_info_from_file(self, file):
        tmp = yaml.load(open(self.config_path + file), Loader=yaml.FullLoader)
        return tmp

    def __normal_login(self, controller=None, username=None, password=None):
        if username is None or password is None:
            username = self.admin_username
            password = self.admin_password
        if controller is None:
            controller = self.main_driver_controller
        actions = [
            {'action': self.action_enum.VISIT, 'url': self.asset['base_url'] + self.urls['login_url']},
            {'action': self.action_enum.TYPE, 'location': self.elements['login_page']['username_box'],
             'content': username},
            {'action': self.action_enum.TYPE, 'location': self.elements['login_page']['password_box'],
             'content': password},
            {'action': self.action_enum.CLICK_ELEMENT, 'location': self.elements['login_page']['login_button']}
        ]
        if (self.asset['kerboeros']):
            print ("using kerboeros")
            actions.insert(1,{'action':self.action_enum.SEND_KEY,'location':'body','key':Keys.ESCAPE})
        controller.act_performance_list(actions)

    def __websudo(self):
        actions = [
            {'action': self.action_enum.VISIT,
             'url': self.asset['base_url'] + self.urls['system_info_path']},
            {'action': self.action_enum.CHECK_ELEM_EXISTS,
             'location': self.elements['login_page']['password_admin_box']},
            {'action': self.action_enum.TYPE, 'location': self.elements['login_page']['password_admin_box'],
             'content': self.admin_password},
            {'action': self.action_enum.CLICK_ELEMENT, 'location': self.elements['login_page']['login_button']}
        ]
        # skip when websudo is diabled
        try:
            self.main_driver_controller.act_performance_list(actions)
        except TimeoutException:
            pass

    def __kill_popups(self, controller=None):
        if (controller == None):
            controller = self.main_driver_controller
        # get ride of the pop ups
        i = 0
        actions = [
            {'action': self.action_enum.CLICK_ELEMENTS, 'location': self.elements['close_popup']},
            {'action': self.action_enum.CLICK_ELEMENTS, 'location': self.elements['message_popup']},
            {'action': self.action_enum.CLICK_ELEMENTS, 'location': self.elements['health_check_popup']}
        ]
        controller.act_performance_list(actions)

    def __find_element(self, location):
        for i in range(0, self.MAX_ATTEMPTS):
            try:
                elem = self.browser.find_element_by_css_selector(location)
                if elem is not None:
                    return elem
                else:
                    self.browser.implicitly_wait(self.WAIT_TIME)
            except Exception as identifier:
                self.browser.implicitly_wait(self.WAIT_TIME)
                pass

    def __delete_dashboard(self):
        self.main_driver_controller.act_performance_list([
            {'action': self.action_enum.VISIT, 'url': self.dashboard_url},
            {'action': self.action_enum.CLICK_ELEMENT,
             'location': self.elements['dashboard']['dashboard_dropdown']},
            {'action': self.action_enum.CLICK_ELEMENT,
             'location': self.elements['dashboard']['dashboard_deletion']},
            {'action': self.action_enum.CLICK_ELEMENT,
             'location': self.elements['dashboard']['confirm_dashboard_deletion']},
        ])

    # Create Project
    def __prep_project__(self):
        data = {
            'key': self.project_key,
            'name': self.project_name,
            'projectTypeKey': 'software',
            'projectTemplateKey': 'com.pyxis.greenhopper.jira:gh-scrum-template',
            'lead': self.asset['admin_username'],
            'description': 'Test Project From DI for post upgrade testing',
            "assigneeType": "PROJECT_LEAD",
        }
        print(self.jira.post('rest/api/2/project', data=data))
        self.test_ticket_list = []
        key = ""
        for i in range(0, self.TEST_TICKET_NUMBER):
            try:
                key = self.jira.create_issue(fields={
                    'project': {'key': self.project_key},
                    'issuetype': {
                        'name': 'Task'
                    },
                    'summary': self.gen.sentence()
                })['key']
                print(key)
            except Exception as identifier:
                pass
            if key[:10] == self.project_key:
                self.test_ticket_list.append(key)
        print(self.test_ticket_list)

    def login(self, user_type=None, user_name=None, password=None):
        self.__normal_login(user_name, password)
        if user_type is None or user_type == 'admin':
            self.__websudo()
        self.__kill_popups()

    # Test 1 Check server logs for error messages
    def test_case_1_check_error_message(self):
        print(inspect.currentframe().f_code.co_name + '\tstarted')

        actions = [
            {'action': self.action_enum.VISIT,
             'url': self.asset['base_url'] + self.urls['troubleshooting_url']},
            {'action': self.action_enum.CLICK_ELEMENT,
             'location': self.elements['troubleshooting']['log_analyer']},
            {'action': self.action_enum.CLICK_IF_EXISTS,
             'location': self.elements['troubleshooting']['refresh_button']}
            # {'action': self.action_enum.WAIT_ELEM,
            #  'location': self.elements['troubleshooting']['urgency_filter']},
            # {'action': self.action_enum.SCROLL_SCREEN_SHOT,
            #  'location': self.elements['troubleshooting']['urgency_filter']}
        ]
        self.main_driver_controller.act_performance_list(actions)

        print(inspect.currentframe().f_code.co_name + '\tfinished, Please take screenshot manully')

    # Test 2 Log in Application
    def test_case_2_checkApplicationUser(self):
        print(inspect.currentframe().f_code.co_name + '\tstarted')
        actions = [
            {'action': self.action_enum.VISIT, 'url': self.asset['base_url'] + self.urls['profile_url']},
            {'action': self.action_enum.NORMAL_SCREEN_SHOT}
        ]

        self.main_driver_controller.act_performance_list(actions)

        print(inspect.currentframe().f_code.co_name + '\tfinished')

    # Test 3 Verify that the application link is still present
    def test_case_3_verify_application_link(self):
        print(inspect.currentframe().f_code.co_name + '\tstarted')
        actions = [
            {'action': self.action_enum.VISIT,
             'url': self.asset['base_url'] + self.urls['applicationlink_url']},
            {'action': self.action_enum.SCROLL_TO_ELEM,
             'location': self.elements['application_link']['application_link_content']},
            {'action': self.action_enum.WAIT_TIME},
            {'action': self.action_enum.NORMAL_SCREEN_SHOT,'waited_time':5}

        ]
        self.main_driver_controller.act_performance_list(actions)

        print(inspect.currentframe().f_code.co_name + '\tfinished')

    # Test 4 Check the application version is shown
    def test_case_4_verifyTargetVersion(self):
        print(inspect.currentframe().f_code.co_name + '\tstarted')

        actions = [
            {'action': self.action_enum.VISIT,
             'url': self.asset['base_url'] + self.urls['system_info_path']},
            {'action': self.action_enum.SCROLL_TO_ELEM, 'location': self.elements['version_check']['jira_info_version']},
            {'action': self.action_enum.NORMAL_SCREEN_SHOT}

        ]
        self.main_driver_controller.act_performance_list(actions)
        print(inspect.currentframe().f_code.co_name + '\tfinished')

    # Test 5 If using external user management, ensure it's enabled
    def test_case_5_verify_user_direcotry(self):
        print(inspect.currentframe().f_code.co_name + '\tstarted')
        actions = [
            {'action': self.action_enum.VISIT, 'url': self.asset['base_url'] + \
                                                      self.urls['user_directory_url']},
            {'action': self.action_enum.NORMAL_SCREEN_SHOT}
        ]
        self.main_driver_controller.act_performance_list(actions)
        print(inspect.currentframe().f_code.co_name + '\tfinished')

    # Test 6 Ensure path to following items are correct from Administration section
    def test_case_6_verify_path(self):
        print(inspect.currentframe().f_code.co_name + '\tstarted')
        actions = [
            {'action': self.action_enum.VISIT,
             'url': self.asset['base_url'] + self.urls['system_info_path']},
            {'action': self.action_enum.SCROLL_TO_ELEM, 'location': self.elements['file_path']},
            {'action': self.action_enum.NORMAL_SCREEN_SHOT}
        ]
        self.main_driver_controller.act_performance_list(actions)
        print(inspect.currentframe().f_code.co_name + '\tfinished')

    # Test 7 List the user groups
    def test_case_7_list_user_groups(self):
        print(inspect.currentframe().f_code.co_name + '\tstarted')
        group_dict = {}
        group_result = self.jira.get_groups(limit=1000)
        total_group = group_result['total']
        group_dict['total'] = total_group
        group_dict['name'] = []
        for group in group_result['groups']:
            group_dict['name'].append(group['name'])
        with open(self.result_path + self.test_case_7_list_user_groups.__name__ + '.yaml', 'w') as output_file:
            yaml.dump(group_dict, output_file, default_flow_style=False)
        print(inspect.currentframe().f_code.co_name + '\tfinished')

    # Test 8 Total number of Projects
    def test_case_8_list_projects(self):
        print(inspect.currentframe().f_code.co_name + '\tstarted')
        project_results = self.jira.get_all_projects(included_archived=True)
        project_dict = {}
        for project in project_results:
            project_dict[project['key']] = project['name']
        with open(self.result_path + self.test_case_8_list_projects.__name__ + '.yaml', 'w') as output_file:
            yaml.dump(project_dict, output_file, default_flow_style=False)
        print(inspect.currentframe().f_code.co_name + '\tfinished')

    # Test 9 Create new Users
    def test_case_9_create_new_user(self):
        print(inspect.currentframe().f_code.co_name + '\tstarted')
        self.jira.delete('rest/api/2/user?username=' +
                         self.asset['test_username'])

        action = [
            {'action': self.action_enum.VISIT, 'url': self.asset['base_url'] + \
                                                      self.urls['create_user']},
            {'action': self.action_enum.TYPE, 'location': self.elements['user_management']['email_box'],
             'content': self.asset['test_username'] + '@local.com'},
            {'action': self.action_enum.TYPE, 'location': self.elements['user_management']['full_name_box'],
             'content': self.asset['test_username']},
            {'action': self.action_enum.TYPE, 'location': self.elements['user_management']['user_name_box'],
             'content': self.asset['test_username']},
            {'action': self.action_enum.TYPE, 'location': self.elements['user_management']['password_box'],
             'content': self.asset['test_password']},
            {'action': self.action_enum.CLICK_ELEMENT,
             'location': self.elements['user_management']['create_user_button']},
            {'action': self.action_enum.NORMAL_SCREEN_SHOT, 'file_name': '_new_user_creation_'}
        ]
        self.main_driver_controller.act_performance_list(action)

        new_user_driver = testingController()
        self.__normal_login(controller=new_user_driver, username=self.asset['test_username'],
                            password=self.asset['test_password'])
        new_user_action = [
            {'action': self.action_enum.VISIT, 'url': self.asset['base_url'] + self.urls['profile_url']},
            {'action': self.action_enum.NORMAL_SCREEN_SHOT, 'file_name': '_new_user_profile'}
        ]

        new_user_driver.act_performance_list(new_user_action)

        print(inspect.currentframe().f_code.co_name + '\tfinished')

    # Test 10 Create a test ticket
    def test_case_10_create_test_ticket(self):
        print(inspect.currentframe().f_code.co_name + '\tstarted')
        action = [
            {'action': self.action_enum.VISIT,
             'url': self.asset['base_url'] + self.urls['create_issue_url']},
            {'action': self.action_enum.SEND_KEY, 'location': self.elements['create_test_issue']['project_box'],
             'key': Keys.BACKSPACE},
            {'action': self.action_enum.TYPE,
             'location': self.elements['create_test_issue']['project_box'],
             'content': self.project_key},
            {'action': self.action_enum.SEND_KEY, 'location': self.elements['create_test_issue']['project_box'],
             'key': Keys.ENTER},
            {'action': self.action_enum.WAIT_TIME},
            {'action': self.action_enum.SEND_KEY, 'location': self.elements['create_test_issue']['issue_type_box'],
             'key': Keys.BACKSPACE},
            {'action':self.action_enum.TYPE,
              'location': self.elements['create_test_issue']['issue_type_box'],
             'content':'Task'
             },
            {'action': self.action_enum.SEND_KEY, 'location': self.elements['create_test_issue']['project_box'],
             'key': Keys.RETURN},
            {'action': self.action_enum.CLICK_ELEMENT,
             'location': self.elements['create_test_issue']['next_button']},
            {'action': self.action_enum.TYPE,
             'location': self.elements['create_test_issue']['summary_box'], 'content': self.gen.sentence()},
            {'action': self.action_enum.SCROLL_TO_ELEM,
             'location': self.elements['create_test_issue']['create_button']},
            {'action': self.action_enum.CLICK_ELEMENT,
             'location': self.elements['create_test_issue']['create_button']},
            {'action': self.action_enum.NORMAL_SCREEN_SHOT,'waited_time': 5}
        ]
        self.main_driver_controller.act_performance_list(action)

        self.ticket_id = self.main_driver_controller.get_current_url().split('/')[-1]

        print(inspect.currentframe().f_code.co_name + '\tfinished')

    # Test 11 Attachment in a ticket
    def test_case_11_attach_to_ticket(self):
        print(inspect.currentframe().f_code.co_name + '\tstarted')
        attachment_path = self.config_path+self.asset['logo_picture']
        self.jira.add_attachment(self.ticket_id, attachment_path)
        actions = [
            {'action': self.action_enum.VISIT,
             'url': self.asset['base_url'] + self.urls['browse_issue_url'] + self.ticket_id},
            {'action': self.action_enum.SCROLL_TO_ELEM,
             'location': self.elements['attachment']['attachment_box']},
            {'action': self.action_enum.NORMAL_SCREEN_SHOT}
        ]
        self.main_driver_controller.act_performance_list(actions)

        print(inspect.currentframe().f_code.co_name + '\tfinished')

    # Test 12 Search for issues
    def test_case_12_search_for_issue(self):
        print(inspect.currentframe().f_code.co_name + '\tstarted')
        self.main_driver_controller.act_performance_list([
            {'action': self.action_enum.VISIT,
             'url': self.asset['base_url'] + self.urls['jql_search_url']},
            {'action': self.action_enum.TYPE, 'location': self.elements['search']['jql_box'],
             'content': 'key=' + self.ticket_id},
            {'action': self.action_enum.SEND_KEY, 'location': self.elements['search']['jql_box'],
             'key': Keys.RETURN},
            {'action': self.action_enum.WAIT_ELEM,
             'location': self.elements['search']['first_result_location']},
            {'action': self.action_enum.NORMAL_SCREEN_SHOT}

        ])
        print(inspect.currentframe().f_code.co_name + '\tfinished')

    # Test 13 Check JIRA gadgets/filters are working
    def test_case_13_check_filter(self):
        print(inspect.currentframe().f_code.co_name + '\tstarted')
        test_jql = "project =  " + self.project_name
        self.main_driver_controller.act_performance_list([
            {'action': self.action_enum.VISIT,
             'url': self.asset['base_url'] + self.urls['jql_search_url']},
            {'action': self.action_enum.TYPE, 'location': self.elements['search']['jql_box'],
             'content': test_jql},
            {'action': self.action_enum.SEND_KEY, 'location': self.elements['search']['jql_box'],
             'key': Keys.RETURN},
            {'action': self.action_enum.WAIT_TIME},
            {'action': self.action_enum.NORMAL_SCREEN_SHOT}
        ])

        print(inspect.currentframe().f_code.co_name + '\tfinished')

    # Test 14 Comment on a ticket
    def test_case_14_comment_on_ticket(self):
        print(inspect.currentframe().f_code.co_name + '\tstarted')
        self.main_driver_controller.act_performance_list([
            {'action': self.action_enum.VISIT,
             'url': self.asset['base_url'] + self.urls['browse_issue_url'] + self.ticket_id},
            {'action': self.action_enum.SCROLL_TO_ELEM, 'location': self.elements['comment']['comment_button']},
            {'action': self.action_enum.CLICK_ELEMENT, 'location': self.elements['comment']['comment_button']},
            {'action':self.action_enum.SCROLL_TO_ELEM,'location':self.elements['comment']['comment_add_button']},
            {'action': self.action_enum.CLICK_ELEMENT,
             'location': self.elements['comment']['comment_shown_as_text']},
            {'action': self.action_enum.TYPE, 'location': self.elements['comment']['comment_box'],
             'content': self.gen.paragraph()},
            {'action': self.action_enum.CLICK_ELEMENT,
             'location': self.elements['comment']['comment_add_button']},
            {'action': self.action_enum.NORMAL_SCREEN_SHOT}
        ])
        print(inspect.currentframe().f_code.co_name + '\tfinished')

    # Test 15 Link tickets together	
    def test_case_15_link_tickets_together(self):
        print(inspect.currentframe().f_code.co_name + '\tstarted')
        test_ticket_key = self.test_ticket_list[random.randrange(len(self.test_ticket_list))]

        actions = [
            {'action': self.action_enum.VISIT,
             'url': self.asset['base_url'] + self.urls['browse_issue_url'] + self.ticket_id},
            {'action': self.action_enum.CLICK_ELEMENT, 'location': self.elements['link_issue']['more_button']},
            {'action': self.action_enum.CLICK_ELEMENT, 'location': self.elements['link_issue']['link_button']},
            {'action': self.action_enum.TYPE, 'location': self.elements['link_issue']['link_issue_box'],
             'content': test_ticket_key},
            {'action': self.action_enum.CLICK_ELEMENT,
             'location': self.elements['link_issue']['confirm_link_button']},
            {'action': self.action_enum.SCROLL_TO_ELEM,
             'location': self.elements['link_issue']['issue_links_list']},
            {'action': self.action_enum.NORMAL_SCREEN_SHOT}
        ]
        self.main_driver_controller.act_performance_list(actions)

        print(inspect.currentframe().f_code.co_name + '\tfinished')

    # Test 17 Create Dashboard
    def test_case_17_create_dashboard(self):
        print(inspect.currentframe().f_code.co_name + '\tstarted')
        actions = [
            {'action': self.action_enum.VISIT,
             'url': self.asset['base_url'] + self.urls['manage_dashboards']},
            {'action': self.action_enum.CLICK_ELEMENT,
             'location': self.elements['dashboard']['create_dashboard_button']},
            {'action': self.action_enum.TYPE, 'location': self.elements['dashboard']['name_box'],
             'content': self.asset['test_dashboard_name']},
            {'action': self.action_enum.CLICK_ELEMENT,
             'location': self.elements['dashboard']['add_dashboard_button']},
            {'action': self.action_enum.CLICK_TEXT, 'content': self.asset['test_dashboard_name']},

            {'action': self.action_enum.CLICK_ELEMENT,
             'location': self.elements['dashboard']['add_gadget_button']},
            {'action': self.action_enum.CLICK_ELEMENT,
             'location': self.elements['dashboard']['load_gadget_button']},
            # {'action': self.action_enum.CLICK_ELEMENT,
            #  'location': self.elements['dashboard']['create_dashboard_button']},
            {'action': self.action_enum.TYPE, 'location': self.elements['dashboard']['gadget_search_box'],
             'content': self.asset['test_gadgets']},
            {'action': self.action_enum.CLICK_ELEMENT,
             'location': self.elements['dashboard']['add_first_gadget_button']},
            {'action': self.action_enum.CLICK_ELEMENT,
             'location': self.elements['dashboard']['close_search_window']},
            {'action': self.action_enum.WAIT_TIME},
            {'action': self.action_enum.CLICK_INTO_IFRAME, 'location': '.button.submit'},
            {'action': self.action_enum.WAIT_TIME},
            {'action': self.action_enum.CLICK_INTO_IFRAME, 'location': 'results-count aui-item'},
            {'action': self.action_enum.NORMAL_SCREEN_SHOT}
        ]
        self.main_driver_controller.act_performance_list(actions)

        self.dashboard_url = self.main_driver_controller.get_current_url()
        print(inspect.currentframe().f_code.co_name + '\tfinished')

    # Test 18 Access Agile Board
    def test_case_18_access_agile_board(self):
        print(inspect.currentframe().f_code.co_name + '\tstarted')
        board_id = self.jira.get_all_agile_boards(project_key=self.project_key)['values'][0]['id']
        self.main_driver_controller.act_performance_list([
            {'action': self.action_enum.VISIT,
             'url': self.asset['base_url'] + '/secure/RapidBoard.jspa?rapidView=' + str(
                 board_id) + '&view=planning&issueLimit=100'},
            # {'action':self.action_enum.CLICK_ELEMENT,'location':'#ghx-pool > div > div > p > a'},
            {'action': self.action_enum.WAIT_ELEM,
             'location': self.elements['agileboard']['backlog_container']},
            {'action': self.action_enum.NORMAL_SCREEN_SHOT}
        ])

        print(inspect.currentframe().f_code.co_name + '\tfinished')

    # Test 20 Verify email settings & notifications
    def test_case_20_verify_email_settings(self):
        print(inspect.currentframe().f_code.co_name + '\tstarted')

        self.main_driver_controller.act_performance_list([
            {'action': self.action_enum.VISIT,
             'url': self.asset['base_url'] + self.urls['outgoing_mail']},
            {'action': self.action_enum.NORMAL_SCREEN_SHOT, 'file_name': '_outgoing_'},
            {'action': self.action_enum.VISIT,
             'url': self.asset['base_url'] + self.urls['incoming_mail']},
            {'action': self.action_enum.NORMAL_SCREEN_SHOT, 'file_name': '_incoming_'},
            {'action': self.action_enum.VISIT,
             'url': self.asset['base_url'] + self.urls['global_mail_setting']},
            {'action': self.action_enum.NORMAL_SCREEN_SHOT, 'file_name': '_global_mail_setting_'},
        ])

        print(inspect.currentframe().f_code.co_name + '\tfinished')

    # Test 22 Ensure existing addons are enabled and working correctly.
    def test_case_22_verify_add_on(self):
        print(inspect.currentframe().f_code.co_name + '\tstarted')

        self.main_driver_controller.act_performance_list([
            {'action': self.action_enum.VISIT, 'url': self.asset['base_url'] + self.urls['manage_apps']},
            {'action': self.action_enum.WAIT_ELEM,
             'location': self.elements['add_on']['user_installed_apps_banner']},
            {'action': self.action_enum.SCROLL_SCREEN_SHOT,
             'location': self.elements['add_on']['upm_manage_container']}
        ])

        print(inspect.currentframe().f_code.co_name + '\tfinished')

    # Test 24 Ensure production licence is up to date
    def test_case_24_verify_license_is_up_to_date(self):
        print(inspect.currentframe().f_code.co_name + '\tstarted')

        self.main_driver_controller.act_performance_list([
            {'action': self.action_enum.VISIT,
             'url': self.asset['base_url'] + self.urls['application_license']},
            {'action': self.action_enum.SCROLL_SCREEN_SHOT,
             'location': self.elements['license']['application_list']}

        ])

        print(inspect.currentframe().f_code.co_name + '\tfinished')

    # Get database stat
    def get_databaes_stat(self):

        self.main_driver_controller.act_performance_list([
            {'action': self.action_enum.VISIT, 'url': self.asset['base_url'] + \
                                                      self.urls['system_info_path']},
            {'action': self.action_enum.WAIT_ELEM, 'location': self.elements['database_stat']['system_info']},
            {'action': self.action_enum.SCROLL_TO_ELEM,
             'location': self.elements['database_stat']['stat_elem']},
        ])

        table_row_item_name_elems = self.main_driver_controller.get_elements_by_css_selector(
            self.elements['database_stat']['table_row_item_name'])
        table_row_item_count_elems = self.main_driver_controller.get_elements_by_css_selector(
            self.elements['database_stat']['table_row_item_data'])
        db_dict = {}
        for (table_row_item_name_elem, table_row_data_elem) in zip(table_row_item_name_elems,
                                                                   table_row_item_count_elems):
            db_dict[table_row_item_name_elem.text] = table_row_data_elem.text
        with open(self.output_database, 'w') as outfile:
            yaml.dump(db_dict, outfile, default_flow_style=False)

    def __get_all_filter(self):
        filter_id_list = []
        header = self.jira.form_token_headers

        soup = BeautifulSoup(
            self.jira.get(path=self.urls['fav_filter'], not_json_response=True, headers=header),
            'html.parser')

        for item in soup.find_all('div', 'favourite-item'):

            hyper_link = item.find('a', href=True)
            name = hyper_link.text
            filter_id = hyper_link.attrs['href'].split('=')[-1]
            if name.startswith('Filter for DITEST2020'):
                filter_id_list.append(filter_id)
        return filter_id_list

    def __delete_filters(self):
        filter_id_list = self.__get_all_filter()
        for filter_id in filter_id_list:
            actions = [
                {'action': self.action_enum.VISIT,
                 'url': self.asset['base_url'] + self.urls['delete_filter'].format(filter_id)},
                {'action': self.action_enum.CLICK_ELEMENT, 'location': '#delete-filter-submit'}
            ]
            self.main_driver_controller.act_performance_list(actions)

    def tear_down(self):
        self.jira.delete('rest/api/2/user?username=' +
                         self.asset['test_username'])
        self.__delete_dashboard()
        self.jira.delete_project(self.project_key)

        self.main_driver_controller.delete_schema(
            url=self.asset['base_url'] + self.urls['issuetypescheme'],
            table_location=self.elements['tear_down']['issuetypescheme']['table'],
            name_location=self.elements['tear_down']['issuetypescheme']['name'],
            project_key=self.project_key,
            button_location_format=self.elements['tear_down']['issuetypescheme']['delete_button'],
            confirm_delete_location=self.elements['tear_down']['issuetypescheme']['confirm_delete']

        )
        self.main_driver_controller.delete_schema(
            url=self.asset['base_url'] + self.urls['issuetypescreenscheme'],
            table_location=self.elements['tear_down']['issuetypescreenscheme']['table'],
            name_location=self.elements['tear_down']['issuetypescreenscheme']['name'],
            project_key=self.project_key,
            button_location_format=self.elements['tear_down']['issuetypescreenscheme']['delete_button'],
            confirm_delete_location=self.elements['tear_down']['issuetypescreenscheme']['confirm_delete']
        )
        self.__delete_filters()


if __name__ == "__main__":
    automation = Automation()
    try:
        automation.test_case_1_check_error_message()
        automation.test_case_2_checkApplicationUser()
        automation.test_case_3_verify_application_link()
        automation.test_case_4_verifyTargetVersion()
        automation.test_case_5_verify_user_direcotry()
        automation.test_case_6_verify_path()
        automation.test_case_7_list_user_groups()
        automation.test_case_8_list_projects()
        automation.test_case_9_create_new_user()
        automation.get_databaes_stat()
        automation.__prep_project__()
        automation.test_case_10_create_test_ticket()
        automation.test_case_11_attach_to_ticket()
        automation.test_case_12_search_for_issue()
        automation.test_case_14_comment_on_ticket()
        automation.test_case_15_link_tickets_together()
        automation.test_case_17_create_dashboard()
        automation.test_case_18_access_agile_board()
        automation.test_case_20_verify_email_settings()
        automation.test_case_22_verify_add_on()
        automation.test_case_24_verify_license_is_up_to_date()
    except Exception as identifier:
        automation.main_driver_controller.driver.save_screenshot('./results/' + 'error.png')
        traceback.print_exc()
        print(identifier)
    finally:
        automation.tear_down()

    # version = automation.verifyTargetVersion()
    # print(version)
